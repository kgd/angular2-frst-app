import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { environment } from './app/';
import { AppModule } from './app/component-overview/app.module';
import {DatabindingModule} from "./app/databinding/databinding.module";
import {LifecycleComponent} from "./app/lifecycle.component";
import {LifecycleModule} from "./app/lifecycle.module";

if (environment.production) {
  enableProdMode();
}

//platformBrowserDynamic().bootstrapModule(AppModule);
//platformBrowserDynamic().bootstrapModule(DatabindingModule)
platformBrowserDynamic().bootstrapModule(LifecycleModule)
