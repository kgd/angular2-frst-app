import {NgModule} from '@angular/core'
import {BrowserModule} from '@angular/platform-browser'
import {LifecycleComponent} from "./lifecycle.component";

@NgModule({
  declarations: [LifecycleComponent],
  imports: [BrowserModule],
  bootstrap: [LifecycleComponent]
})
export class LifecycleModule {
}
