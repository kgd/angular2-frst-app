import {NgModule} from '@angular/core'
import {BrowserModule} from '@angular/platform-browser'
import { FormsModule } from '@angular/forms';


import {DatabindingComponent} from './databinding.component'
import {PropertyBindingComponent} from './property-binding.component'
import {EventBindingComponent} from './event-binding.component'
import {TwowayBindingComponent} from "./twoway-binding.component";

@NgModule({
  declarations: [DatabindingComponent, PropertyBindingComponent, EventBindingComponent, TwowayBindingComponent],
  imports: [BrowserModule, FormsModule],
  bootstrap: [DatabindingComponent]
})
export class DatabindingModule {
}
