import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'app-event-binding',
  template: `
    <button (click)="onClick()">Click me!</button>
  `,
  styles: []
})
export class EventBindingComponent{

  /*nie zmieniaj nazwy jesli nie trzeba*/
  @Output('clickable') clicked = new EventEmitter<string>();

  onClick(){
    this.clicked.emit('It works');
  }
}
