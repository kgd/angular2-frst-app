import {Component} from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'databinding',
  templateUrl: 'databinding.html',
  styleUrls: ['databinding.css']
})

export class DatabindingComponent {
  stringInterpolation: string = 'String interpolation';
  numberInterpolation: number = 4;

  onTest(){
    return true;
  }

  onClicked(value: string){
    alert(value);
  }
}
