import { Component } from '@angular/core';
import {FORM_DIRECTIVES} from '@angular/common';

@Component({
  moduleId: module.id,
  selector: 'app-twoway-binding',
  template: `
    <input type="text" [(ngModel)]="person.name">
    <input type="text" [(ngModel)]="person.name">
  `,
  styles: []
})
export class TwowayBindingComponent  {
  person = {
    name: "Krz",
    age: "23"
  };

}
