import {Component} from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'app-another',
  template: `
    <article>
      <ng-content></ng-content>  
    </article>
  `,
  styles: [`
  p { color : red
     }
  article {
      border: 1px solid black
  }
  `]
})
export class AnotherComponent {

}
