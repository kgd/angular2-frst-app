import { FistAppPage } from './app.po';

describe('fist-app App', function() {
  let page: FistAppPage;

  beforeEach(() => {
    page = new FistAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
